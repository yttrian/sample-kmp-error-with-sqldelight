plugins {
    // applying sqldelight to have sqlite included without the linker option
    // using the -lsqlite3 linker option and not applying sqldelight still results in failed builds
    alias(libs.plugins.sqldelight)
}
