import androidx.compose.ui.window.ComposeUIViewController
import sample.SampleApp

fun ViewController() = ComposeUIViewController { SampleApp() }
