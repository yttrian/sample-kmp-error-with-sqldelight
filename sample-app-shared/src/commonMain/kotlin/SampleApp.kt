package sample

import androidx.compose.runtime.Composable

/**
 * App entry point
 */
@Composable
expect fun SampleApp()
