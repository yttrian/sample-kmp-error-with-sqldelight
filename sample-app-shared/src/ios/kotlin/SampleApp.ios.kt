package sample

import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import sample.data.Database
import sample.data.Sample

/**
 * App entry point
 */
@Composable
actual fun SampleApp() {
    Text("Hello iOS! I have ${Database::class.qualifiedName} and ${Sample::class.qualifiedName}!")
}
