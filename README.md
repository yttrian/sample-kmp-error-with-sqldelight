# Sample KMP Error

`gradle sample-app-android:build` works but
`gradle sample-app-ios:build` fails!

## The Error

```
KLIB resolver: The same 'unique_name=runtime_commonMain' found in more than one library: /[removed]/sample-app-shared/build/kotlinTransformedMetadataLibraries/commonMain/app.cash.sqldelight-runtime-2.0.2-commonMain-asQ-DQ.klib, /[removed]/sample-app-shared/build/kotlinTransformedMetadataLibraries/commonMain/org.jetbrains.compose.runtime-runtime-1.6.10-commonMain-CVJWAg.klib
e: file:///[removed]/sample-app-shared/src/ios/kotlin/SampleApp.ios.kt:4:33 Unresolved reference: Composable
e: file:///[removed]/sample-app-shared/src/ios/kotlin/SampleApp.ios.kt:11:2 Unresolved reference: Composable
e: file:///[removed]/sample-app-shared/src/ios/kotlin/SampleApp.ios.kt:12:12 Functions which invoke @Composable functions must be marked with the @Composable annotation
e: file:///[removed]/sample-app-shared/src/ios/kotlin/SampleApp.ios.kt:12:12 Mismatched @Composable annotation between expect and actual declaration
e: file:///[removed]/sample-app-shared/src/ios/kotlin/SampleApp.ios.kt:13:5 @Composable invocations can only happen from the context of a @Composable function
```
